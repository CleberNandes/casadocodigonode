var express = require('../config/express')();
var request = require('supertest')(express);
	// adicionar os comando no scripts do package.json
	//"start": "set NODE_ENV=test node_modules/mocha/bin/mocha",
    //"test": "echo \"Error: no test specified\" && exit 1",
    //"start": "set NODE_ENV=test",
    //"test": "mocha",
    //"start": "set NODE_ENV=test&&mocha --reporter spec",
    //"start": "set node_modules/mocha/bin/mocha"

describe('ProdutosController',function(){

	// sempre que ele for executar os testes ele vai limpar o banco
	/* beforeEach(function(done){
		var conn = express.database.mysqlFactory();
		conn.query('delete from livros',function(ex,result){
			if(!ex){
				done();
			}
		});
		conn.query('alter table livros auto_increment = 1');
	}); */

	it('#LISTAGEM JSON',function(done){
		request.get('/produtos')
		.set('Accept','application/json')
		.expect('Content-Type',/json/)
		.expect(200,done);

	});

	it('#LISTAGEM HTML',function(done){
		request.get('/produtos')
		.set('Accept','text/html')
		.expect('Content-Type',/html/)
		.expect(200,done);

	});

	it('#CADASTRO DE NOVO PRODUTO COM DADOS INVALIDOS',function(done){
		request.post('/produtos')
		.send({titulo:"",descricao:"",preco:""})
		.expect(400,done);

	});

	it('#CADASTRO DE NOVO PRODUTO COM DADOS VALIDOS',function(done){
		request.post('/produtos')
		.send({titulo:"Nome novo",descricao:"novo@func",preco:"123.32"})
		.expect(302,done);
	});

	//node-database-cleaner pesquisar sobre essa lib feita por brasilerio

});
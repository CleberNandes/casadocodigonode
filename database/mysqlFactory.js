var mysql = require("mysql");
    
function createMysqlConnection(){
    console.log(process.env.NODE_ENV);
    
    if(process.env.NODE_ENV == 'test'){
        return mysql.createConnection({
            host: "localhost",
            user: "root",
            password: "123456",
            database: "casadocodigo_test"
        });
    }
    // se não for passada nenhuma variavel de ambiente será dev
    // process.env.NODE_ENV == 'development'
    if (!process.env.NODE_ENV || process.env.node === 'dev') {
        return mysql.createConnection({
            host: "localhost",
            user: "root",
            password: "123456",
            database: "casadocodigo_node"
        });
    }

    if (process.env.NODE_ENV == "production") {
      var url = process.env.CLEARDB_DATABASE_URL;
      var grupos = url.match(/mysql:\/\/(.*):(.*)@(.*)\/(.*)\?/);
      return mysql.createConnection({
        host: grupos[3],
        user: grupos[1],
        password: grupos[2],
        database: grupos[4]
      });
    }

    /* if (process.env.node === "production") {
      return mysql.createConnection({
        host: "us-cdbr-iron-east-04.cleardb.net",
        user: "b6a26df2dee280",
        password: "4cb96adf",
        database: "heroku_704e41e7162a0c2"
      });
    } */
        
};

//wrapper
module.exports = function(){
    return createMysqlConnection;
};
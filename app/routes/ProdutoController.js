const { check, validationResult } = require("express-validator/check");
const { matchedData, sanitize } = require("express-validator/filter");

module.exports = function(app){    
	
	app.get("/produtos", function(req, res, next) {
		var conn = app.database.mysqlFactory();
		var produtosDao = new app.app.dao.ProdutoDao(conn);
		
		produtosDao.lista(function(err, result){
			// caso tenha algum erro retorna não deixando passar no text
			if(err){
				console.log(err);
				return next(err);
			}
			//res.send(result);
			res.format({
				html: function() {
					res.render("produto/lista",{lista:result,erros:[],produto:{}});
				},
				json: function(){
					res.json(result);
				}
			});
		});  
		conn.end();
	});

	app.get('/produtos/form', function(req,res){
		res.render('produto/form',{produto:{},erros:[]});
	});
	
	app.post('/produtos',function(req, res, next){

		//req.check('titulo').isEmail();
		
		req.assert("titulo", "Título é obrigatorio").notEmpty();
		req.assert("preco", "Formato inválido").isFloat();
		req.assert("descricao", "Formato inválido").notEmpty();
		var erros = req.validationErrors();
		var produto = req.body;

		//res.send(erros);

		if(erros){			
    	res.format({ 
				html: function() {
					res.status(400).render("produto/form", {
						erros: erros,produto: produto
					});
				}, 
				json: function() {
					res.status(400).json(erros);
				} });
			return;
		}

		var conn = app.database.mysqlFactory();
		var produtosDao = new app.app.dao.ProdutoDao(conn);
		
		
		
		produtosDao.create(produto, function(err, result){
			if (err) {
				console.log(err);
				return next(err);
			}
			res.format({ 
				html: function() {
					res.redirect("/produtos");
				}, json: function() {
					res.json(result);
				} 
			});
			
		});
		conn.end();
	});
};
	
module.exports = function(app) {
  app.get("/", function(req, res, next) {
    var conn = app.database.mysqlFactory();
    // sempre dar new para instanciar um dao
    var produtos = new app.app.dao.ProdutoDao(conn);

    produtos.lista(function(err, results) {
      if (err) {
        console.log('erro home '+err);
        return next(err);
      }
      res.render("home/index", { livros: results });
    });
    conn.end();
  });
};

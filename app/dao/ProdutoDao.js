function ProdutoDao(conn){
	this._conn = conn;
}


ProdutoDao.prototype.lista = function(callback) {
	this._conn.query("select * from livros", callback);
}

ProdutoDao.prototype.create = function(produto, callback){
	//o driver do mysql fornece o set
	this._conn.query('insert into livros set ?',produto,callback);
}

module.exports = function() {
	return ProdutoDao;
};
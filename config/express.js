var express = require("express");
var load = require('express-load');
// preenche os campos na requisição
var bodyParser = require('body-parser');
var validator = require('express-validator');

module.exports = function() {   
    var app = express();
    // determina a pasta public para os arquivos publicos
    app.use(express.static("./public"));
    app.set("view engine", "ejs");
    app.set("views", "./views");

    //middleware
    //usa body-parser para as requisições
    app.use(bodyParser.urlencoded({extended:true}));
    // urlencoded é o formato que o form envia os dados
    //extended:true permite enviar formularios mais complexos
    // middleware para requisições de clients em json format
    app.use(bodyParser.json());
    app.use(validator());

    

    //usando auto load dentro da app
    load('app').then('database').into(app);

    //middleware que trata rotas não achadas
    // tem que ficar por ultimo, primerio ele deve procurar nas rotas
    app.use(function(req,res,next){
        res.status(404).render('erro/404');
        next();
    });

    app.use(function(error, req, res, next) {
      if (process.env.NODE_ENV == "production") {

        //res.status(500).send(error);
        res.status(500).render("erros/500");
        return;
      }
      next(error);
    });

    return app;
};

